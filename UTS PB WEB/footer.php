<footer class="row mt-5 mb-5">
	<div class="col-lg-12">
		<p class="text-center tm-text-gray tm-copyright-text mb-0">Copyright &copy;
			<span class="tm-current-year">2021</span> <a href="#" class="tm-text-white"> Farhan Bagas S </a> | UTS : PB WEB
		</p>
	</div>
</footer>