<!DOCTYPE html>
<html lang="en">
<head>
	<?php include "function.php"; ?>
</head>
<body>
<div class="container">
        <div class="row">
            <div class="col-lg-12">
                <header class="text-center tm-site-header">
                    <div class="tm-site-logo"></div>
                    <h1 class="pl-4 tm-site-title">Satriakusumas's Guide</h1>
                </header>
            </div>
        </div>
        <div class="container tm-container-2">
            <div class="row">
                <div class="col-sm-12">
					<button type="button" class="btn btn-info add-new"><i class="fa fa-plus"></i> ADD</button>
                </div>
            </div>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>ID</th>
						<th>Name</th>
                        <th>Gender</th>
                        <th>Age</th>
                        <th>Country</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>ID00190021</td>
                        <td>Farhan Bagas Satriakusuma</td>
                        <td>Male</td>
						<td>21</td>
						<td>Indonesia</td>
                        <td>
                            <?php include "material.php"; ?>
                        </td>
                    </tr>  
                </tbody>
            </table>
        </div>
    </div>
</body>
    <?php include "footer.php"; ?>
</html>